# -*- coding: UTF-8 -*-
import wmi
from typing import (
    Dict,
    List
)
from .MetaDataExceptions import *


class MetaData(object):
    """
    Esta Classe é responsável por coletar meta informações de arquivos
    do sistema operacional.
    :ref    https://msdn.microsoft.com/en-us/library/aa387236(v=vs.85).aspx
    """

    def __init__(self, _wmi_namespace: wmi._wmi_namespace) -> None:
        """
        Inicia a classe com uma instancia de wmi._wmi_namespace válida
        :param _wmi_namespace:      Uma instancia de wmi._wmi_namespace
        :return:                    None
        """
        if not isinstance(_wmi_namespace, wmi._wmi_namespace):
            raise NotValidWmiNamespaceInstance("A instância de Service não recebeu um wmi_namespace válido")
        self.__wmi = _wmi_namespace

    def list(self, fields: list = [], **where_clause) -> List:
        """
        Método para listar metainformações de arquivos do
        sistema
        :param fields:          Uma lista de campos
        :param where_clause:    Conjunto chave/valor para condicionais
                                de filtro
        :return:                Uma lista com objetos wmi._wmi_object
        """
        if len(where_clause) == 0:
            raise ValueError(
                'É necessário um filtro para a consulta de metadados, caso contrério o sistema coletará metadados'
                'de todos os arquivos do sistema.'
            )

        field_list = ", ".join(fields) or "*"
        wql = "SELECT " + field_list + " FROM CIM_DataFile"
        if where_clause:
            wql += " WHERE " + " AND ".join(["%s = %r" % (k, str(v)) for k, v in where_clause.items()])

        wql = wql.replace('\\\\', '\\')

        list_datafiles = []
        for datafile in self.__wmi.query(wql):
            list_datafiles.append(datafile)

        return list_datafiles

    def to_dict(self, cim_datafile: List) -> List[Dict]:
        """
        Método para coletar os serviços do sistema

        :see:    Methods __wmi_object_to_dict, properties
        """

        metadata_list = []

        for metadata in cim_datafile:
            metadata_dict = self.__wmi_object_to_dict(metadata)
            metadata_list.append(metadata_dict)

        return metadata_list

    def read_file(self, file_path: str) -> Dict:
        """
        Carrega um arquivo diretamente para coletar meta informações
        :param file_path:       Uma str com o caminho completo para
                                o arquivo.
        :return:                Um dicionário com os metadados do
                                arquivo.
        """
        if not isinstance(file_path, str):
            raise ValueError(
                'O nome o arquivo deve ser uma string com o caminho completo para o arquivo'
            )
        return self.__wmi_object_to_dict(self.list(Name=file_path)[0])

    def __wmi_object_to_dict(self, wmi_object: wmi._wmi_object) -> Dict:
        """
        Método privado para converter um objeto wmi._wmi_object
        em um dicionario com as propriedades da classe Win32_Service
        Para saber quais campos será retornado neste dicionario use
        o método properties

        :param wmi_object:  Uma instancia de wmi._wmi_object
        :return:            Dicionario com as propriedades da classe

        """
        if not isinstance(wmi_object, wmi._wmi_object):
            raise TypeError(
                'O parametro do método __wmi_object_to_dict deve ser uma instancia de wmi._wmi_object'
            )

        properties_names = wmi_object.__dict__['properties'].keys()
        properties_values = [getattr(wmi_object, property_name) for property_name in properties_names]
        return dict(
            zip(
                properties_names,
                properties_values
            )
        )

