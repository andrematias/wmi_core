# -*- coding: UTF-8 -*-
import wmi

from typing import (
    List,
    Dict
)

from .ServicesExceptions import (
    NotValidWmiNamespaceInstance,
    ServicePropertyNotFound,
    StateServicesError,
    LoadServicesError
)


class Service(object):
    """
    Classe responsável por abstrair objetos de serviços
    do sistema operacional.
    """

    def __init__(self, _wmi_namespace: wmi._wmi_namespace) -> None:
        """
        Inicia a classe com uma instancia de wmi._wmi_namespace válida
        :param _wmi_namespace:      Uma instancia de wmi._wmi_namespace
        :return:                    None
        """
        if not isinstance(_wmi_namespace, wmi._wmi_namespace):
            raise NotValidWmiNamespaceInstance("A instância de Service não recebeu um wmi_namespace válido")
        self.__wmi = _wmi_namespace

    def list(self, fields: List = [], **where_clauses) -> List[wmi._wmi_object]:
        """
        Método para listar os serviços correntes no sistema

        :param fields           (Opcional) Uma lista com as colunas
        :param where_clauses    (Opcional) Parametros nomeados cujo as chaves são opcionais e caso
                                utilizados devem estar entre propriedades da classe
                                Win32_Service, que pode ser obtida através da propriedade

        :return:                Lista com um dicionario com informações do serviço
        """
        try:
            self.__check_where_clause(where_clauses)
            return self.__wmi.Win32_Service(fields, **where_clauses)
        except Exception as ex:
            raise LoadServicesError(ex)

    def load(self, win32_service: wmi._wmi_object) -> wmi._wmi_object:
        """
        Método para carregar uma instancia de Win32_Service para
        a instancia de Service
        :param win32_service:
        :return:
        """
        self.__check_is_valid_win32_service(win32_service)
        return self.list(Name=win32_service.Name)[0]

    def to_dict(self, win32_services: List) -> List[Dict]:
        """
        Método para coletar os serviços do sistema

        :see:                   Methods __wmi_object_to_dict, properties
        """

        services = []

        for service in win32_services:
            service_dict = self.__wmi_object_to_dict(service)
            services.append(service_dict)

        return services

    def __check_is_valid_win32_service(self, service: wmi._wmi_object) -> bool:
        """
        Verifica se o serviço é uma instancia válida de _wmi_object

        :param service:     Uma instancia de wmi._wmi_object
        :return:            bool
        :raise:             TypeError
        """
        if not isinstance(service, wmi._wmi_object):
            raise TypeError(
                'O serviço disponibilizado não é uma instancia válida de Win32_Service'
            )
        return True

    def __check_where_clause(self, where_clauses) -> None:
        """
        Verifica se as chaves das clausulas estão presentes na
        propriedade da classe Win32_Service

        :see:                   self.properties
        :param where_clauses:   Um kwargs com paramtros de chave/valor
        :return:                None
        """
        for clause in where_clauses:
            if clause.title() not in self.properties:
                raise ServicePropertyNotFound(
                    "A clausula '%s' não esta entre as propriedades da classe Win32_Service" % clause
                )

    def __wmi_object_to_dict(self, wmi_object: wmi._wmi_object) -> Dict:
        """
        Método privado para converter um objeto wmi._wmi_object
        em um dicionario com as propriedades da classe Win32_Service
        Para saber quais campos será retornado neste dicionario use
        o método properties

        :param wmi_object:  Uma instancia de wmi._wmi_object
        :return:            Dicionario com as propriedades da classe

        """
        if not isinstance(wmi_object, wmi._wmi_object):
            raise TypeError(
                'O parametro do método __wmi_object_to_dict deve ser uma instancia de wmi._wmi_object'
            )

        properties_names = wmi_object.__dict__['properties'].keys()
        properties_values = [getattr(wmi_object, property_name) for property_name in properties_names]
        return dict(
            zip(
                properties_names,
                properties_values
            )
        )

    @property
    def properties(self) -> List:
        """
        Retorna um dicionário com os campos de uma instancia de
        Win32_Service
        :return:
        """
        return list(self.list()[0].properties)


class ServiceManager(object):
    """
    Classe responsável por manusear serviços do sistema.
    Nesta classe esta contido os métodos da classe do WMI Win32_Service.
    :ref        https://msdn.microsoft.com/en-us/library/gg196691(v=vs.85).aspx
    """

    def __init__(self, service: wmi._wmi_object) -> None:
        if not isinstance(service, wmi._wmi_object):
            raise NotValidWmiNamespaceInstance("A instância de ServiceManager não recebeu um Service válido")
        self.__service = service

    def start_service(self) -> bool:
        """
        Inicia um serviço existente no sistema
        :ref                    https://msdn.microsoft.com/en-us/library/aa393660(v=vs.85).aspx
        :return:                bool
        """
        try:

            if self.__service.Started:
                return True

            rcode, = self.__service.StartService()
            self.__format_return_code(rcode)
            return True
        except Exception as ex:
            raise StateServicesError(ex)

    def stop_service(self) -> bool:
        """
        Para um serviço existente no sistema
        :ref                    https://msdn.microsoft.com/en-us/library/aa393673(v=vs.85).aspx
        :return:                bool
        """
        try:

            if self.__service.State == "Stopped":
                return True

            if not self.__service.AcceptStop:
                raise StateServicesError(
                    'O serviço não permite ser parado'
                )

            rcode, = self.__service.StopService()
            self.__format_return_code(rcode)

            return True
        except Exception as ex:
            raise StateServicesError(ex)

    def pause_service(self) -> bool:
        """
        Pausa um serviço existente no sistema
        :ref                    https://msdn.microsoft.com/en-us/library/aa392733(v=vs.85).aspx
        :return:                bool
        """
        try:
            if not self.__service.Started:
                raise StateServicesError(
                    'O serviço não esta iniciado'
                )

            if self.__service.State == "Paused":
                return True

            if not self.__service.AcceptPause:
                raise StateServicesError(
                    'O serviço não permite pausar'
                )
            rcode, = self.__service.PauseService()
            self.__format_return_code(rcode)
            return True
        except Exception as ex:
            raise StateServicesError(ex)

    def resume_service(self) -> bool:
        """
        Despausa um serviço existente no sistema
        :ref                    https://msdn.microsoft.com/en-us/library/aa393234(v=vs.85).aspx
        :return:                bool
        """
        try:
            if not self.__service.Started:
                raise StateServicesError(
                    'O serviço não esta iniciado'
                )

            if "Paused" != self.__service.State != "Stopped":
                return True

            if not self.__service.AcceptPause:
                raise StateServicesError(
                    'O serviço não permite pausar'
                )
            rcode, = self.__service.ResumeService()
            self.__format_return_code(rcode)
            return True
        except Exception as ex:
            raise StateServicesError(ex)

    def __format_return_code(self, return_code: int) -> bool:
        """
        Formata os códigos de retorno do sistema
        :ref                        https://msdn.microsoft.com/en-us/library/aa384901(v=vs.85).aspx
        :param return_code:         Um inteiro com o retorno do sistema
        :return:                    bool
        :raise:                     OSError
        """

        codes = {
            0: 'O pedido foi aceito.',
            1: 'A solicitação não é suportada.',
            2: 'O usuário não teve o acesso necessário',
            3: 'O serviço não pode ser interrompido porque outros serviços em execução dependem dele.',
            4: 'O código de controle solicitado não é válido ou é inaceitável para o serviço.',
            5: 'O código de controle solicitado não pode ser enviado para o serviço porque o estado do serviço '
               '(propriedade Win32_BaseService.State) é igual a 0, 1 ou 2..',
            6: 'O serviço não foi iniciado.',
            7: 'O serviço não respondeu à solicitação de início em tempo hábil.',
            8: 'Falha desconhecida ao iniciar o serviço.',
            9: 'O caminho do diretório para o arquivo executável do serviço não foi encontrado',
            10: 'O serviço já está em execução.',
            11: 'O banco de dados para adicionar um novo serviço está bloqueado.',
            12: 'Uma dependência em que esse serviço se baseia foi removida do sistema.',
            13: 'O serviço não conseguiu encontrar o serviço necessário de um serviço dependente.',
            14: 'O serviço foi desativado do sistema.',
            15: 'O serviço não possui a autenticação correta para ser executada no sistema.',
            16: 'Este serviço está sendo removido do sistema.',
            17: 'O serviço não possui thread de execução.',
            18: 'O serviço tem dependências circulares quando é iniciado.',
            19: 'Um serviço está sendo executado com o mesmo nome.',
            20: 'O nome do serviço tem caracteres inválidos.',
            21: 'Parâmetros inválidos foram passados ​​para o serviço.',
            22: 'A conta na qual este serviço é executado é inválida ou não possui as '
                'permissões para executar o serviço.',
            23: 'O serviço existe no banco de dados de serviços disponíveis no sistema.',
            24: 'O serviço está atualmente em pausa no sistema.',
        }

        if 0 < return_code < 25:
            rc = codes[return_code]
            raise OSError(rc)
        elif return_code > 24:
            raise OSError('Falha inesperada ao traduzir o código de retorno do sistema')
        return True
