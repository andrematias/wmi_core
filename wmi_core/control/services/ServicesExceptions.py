class LoadServicesError(Exception):
    pass


class StateServicesError(Exception):
    pass


class NotValidWmiNamespaceInstance(Exception):
    pass


class ServicePropertyNotFound(Exception):
    pass

