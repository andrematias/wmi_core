# -*- coding: UTF-8 -*-
import wmi
import os
import re
import win32api
from typing import (
    List,
    Tuple,
    Dict,
)
from .RegistryExceptions import (
    NotValidWmiNamespaceInstance,
    NoAccessInKeyOrRegister,
    EntryNotFound,
    MaxLength,
    ExportError,
    ErrorRegistyFormat,
    RegImportError
)


class RegKey(object):
    """
    Classe RegKey
    Objeto que representa uma KEY do registro do Windows
    StdRegProv
    ref: https://msdn.microsoft.com/en-us/library/aa393664(v=vs.85).aspx
    ReturnCodes Microsoft
    ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
    """
    # Tipos de chaves do windows
    HKEY_CLASSES_ROOT = 2147483648
    HKEY_CURRENT_USER = 2147483649
    HKEY_LOCAL_MACHINE = 2147483650
    HKEY_USERS = 2147483651
    HKEY_CURRENT_CONFIG = 2147483653

    # Tipos de permissões nas chaves
    KEY_QUERY_VALUE = 1
    KEY_SET_VALUE = 2
    KEY_QUERY_SET_VALUE = 3
    KEY_CREATE_SUB_KEY = 4
    KEY_ENUMERATE_SUB_KEYS = 8
    KEY_NOTIFY = 16
    KEY_CREATE = 32
    DELETE = 65536
    READ_CONTROL = 131072
    WRITE_DAC = 262144
    WRITE_OWNER = 524288
    KEY_WOW64_64KEY = 256
    KEY_WOW64_32KEY = 512
    KEY_READ = 131097

    def __init__(self, h_key: int, sub_key: str, u_required: int = KEY_QUERY_SET_VALUE) -> None:
        """
        Método de inicialização do objeto da RegKey
        Inicia as propriedades do objeto refernciando ao
        key do registro do windows
        StdRegProv
        ref: https://msdn.microsoft.com/en-us/library/aa393664(v=vs.85).aspx
        ReturnCodes Microsoft
        ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx

        :param h_key:       Recebe uma constante com o inteiro que refere-se
                            a chave HKEY_ do windows
                            ref: https://msdn.microsoft.com/en-us/library/windows/desktop/aa384911(v=vs.85).aspx
        :param sub_key:     Recebe uma string como a subkey registrada dentro de h_key
        :param u_required:  Recebe um int para as chaves de permissões da api do windows
                            o valor default é KEY_QUERY_SET_VALUE = 3.
                            ref: https://msdn.microsoft.com/en-us/library/aa384911(v=vs.85).aspx
        """
        self.h_key = h_key
        self.sub_key = sub_key
        self.u_required = u_required

    def get_hkey_name(self, hkey_code: int) -> str:
        """
        Método para retornar o nome da hkey_ correspondente ao código
        :param hkey_code:   Um código que corresponda a um HKEY_ da classe
        :return:    string com o nome
        """
        constants = {value: name for name, value in vars(self.__class__).items() if name.isupper() if 'HKEY_' in name}
        if hkey_code not in constants or type(hkey_code) != int:
            raise ValueError('o valor informado não corresponde a um atributo ou não é um int')
        return constants[hkey_code]


class RegValue(object):
    """
    Classe RegValue
    Esta classe é responsável por representar um valor
    de um registro do windows
    """

    # Tipos de Registros do windows
    REG_SZ = 1
    REG_EXPAND_SZ = 2
    REG_BINARY = 3
    REG_DWORD = 4
    REG_MULTI_SZ = 7
    REG_QWORD = 11

    def __init__(self, reg_name: str, reg_value: object, reg_type: int = REG_SZ) -> None:
        """
        :param reg_name:    O nome do registro da chave
        :param reg_value:   O valor a ser configurado em um registro, o tipo
                            pode variar de acordo com o reg_type. Exemplo:

                            REG_MULTI_SZ: lista com as strings.
                            ref: https://msdn.microsoft.com/en-us/library/aa393465(v=vs.85).aspx

                            REG_SZ: uma string
                            ref: https://msdn.microsoft.com/en-us/library/aa393600(v=vs.85).aspx

                            REG_DWORD: Um inteiro com até 32 bits
                            ref: https://msdn.microsoft.com/en-us/library/aa393297(v=vs.85).aspx

                            REG_QWORD: Um inteiro com até 64 bits
                            ref: https://msdn.microsoft.com/en-us/library/aa393590(v=vs.85).aspx

                            REG_BINARY: Um inteiro com até 8 bits
                            ref: https://msdn.microsoft.com/en-us/library/aa393286(v=vs.85).aspx

                            REG_EXPAND_SZ: Uma String
                            ref: https://msdn.microsoft.com/en-us/library/aa393299(v=vs.85).aspx

        :param reg_type:    Um tipo int seguindo os padrões do Win32
                            ref: https://msdn.microsoft.com/en-us/library/aa390388(v=vs.85).aspx
        """
        self.reg_value = reg_value
        self.reg_type = reg_type
        self.reg_name = reg_name


class Registry(object):
    """
        Classe Registry
        Controla todos os eventos de leitura e escrita de registros do windows
        StdRegProv
        ref: https://msdn.microsoft.com/en-us/library/aa393664(v=vs.85).aspx
        ReturnCodes Microsoft
        ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
        Tipos de valores
        ref: http://www.informit.com/articles/article.aspx?p=1378466&seqNum=4
    """

    def __init__(self, _wmi_namespace: object) -> None:
        """
        Inicializador da instância da classe
        :param _wmi_namespace: Uma instancia da classe WMI com o namespace root\default
        """
        if not isinstance(_wmi_namespace, wmi._wmi_namespace):
            raise NotValidWmiNamespaceInstance("A instância de Registy não recebeu um wmi_namespace válido")

        self.__StdRegProv = _wmi_namespace.StdRegProv

    def __check_reg_access(self, reg_key: RegKey) -> bool:
        """
        Método privado para verificar se o usuário contém o acesso
        a um registro do Windows
        ref: https://msdn.microsoft.com/en-us/library/aa384911(v=vs.85).aspx

        :return: bool       Retorna True se o usuário tiver permissões de acesso ao registro
        """
        result, grants = self.__StdRegProv.CheckAccess(
            hDefKey=reg_key.h_key,
            sSubKeyName=reg_key.sub_key,
            uRequired=reg_key.u_required
        )

        return result

    def create_key(self, regkey: RegKey) -> bool:
        """
        Método para criar novas chaves de registros do windows
        :param regkey: Uma instancia da classe RegKey
        :return: bool
        """
        rcode, = self.__StdRegProv.CreateKey(
            hDefKey=regkey.h_key,
            sSubKeyName=regkey.sub_key
        )

        return self.__format_return_code(rcode)

    def delete_key(self, regkey: RegKey) -> bool:
        """
        Método para deletar chaves de registro no windows.
        Se a chave conter subchaves será apagadas recursivamente
        ref: https://msdn.microsoft.com/en-us/library/aa389869(v=vs.85).aspx
        :param regkey: Uma instancia de RegKey
        :return: bool
        """

        if not self.__check_reg_access(regkey):
            raise NoAccessInKeyOrRegister(
                "Acesso Negado ou Chave inválida para o registro: {} em {}".format(
                    regkey.sub_key,
                    regkey.get_hkey_name(regkey.h_key)
                )
            )

        subkeys = self.get_keys_names(regkey)
        if len(subkeys) > 0:
            for key in subkeys:
                self.delete_key(key)

        rcode, = self.__StdRegProv.DeleteKey(
            hDefKey=regkey.h_key,
            sSubKeyName=regkey.sub_key
        )

        return self.__format_return_code(rcode)

    def get_keys_names(self, regkey: RegKey) -> List[RegKey]:
        """
        Método para coletar os nomes das chaves registradas em um
        HKEY_... com subkey

        ref: https://msdn.microsoft.com/en-us/library/aa390387(v=vs.85).aspx

        :param regkey:  Uma instancia de RegKey
        :return:        Lista com os nomes das chaves de um registro
        """
        if not self.__check_reg_access(regkey):
            raise NoAccessInKeyOrRegister(
                "Acesso Negado ou Chave inválida para o registro: {} em {}".format(
                    regkey.sub_key,
                    regkey.get_hkey_name(regkey.h_key)
                )
            )

        rcode, key_names = self.__StdRegProv.EnumKey(
            hDefKey=regkey.h_key,
            sSubKeyName=regkey.sub_key
        )

        list_regkey = []

        if self.__format_return_code(rcode):
            for name in key_names:
                sub_regkey = RegKey(
                    regkey.h_key,
                    "{base_sub_key}\{name}".format(
                        base_sub_key=regkey.sub_key,
                        name=name)
                )
                list_regkey.append(sub_regkey)
                inherited_key = self.get_keys_names(sub_regkey)

                if len(inherited_key) > 0:
                    list_regkey += inherited_key

        return list_regkey

    def get_value_name(self, regkey: RegKey) -> List[Tuple[str, int]]:
        """
        Método para coletar todos os nomes das entradas de um
        registro do windows

        ref: https://msdn.microsoft.com/en-us/library/aa390388(v=vs.85).aspx

        :param regkey:      Uma instância de RegKey
        :return:            Objeto list com uma tupla com o Nome e
                            o Tipo do valor, respectivamente
        """
        if not self.__check_reg_access(regkey):
            raise NoAccessInKeyOrRegister(
                "Acesso Negado ou Chave inválida para o registro: {} em {}".format(
                    regkey.sub_key,
                    regkey.get_hkey_name(regkey.h_key)
                )
            )

        rcode, entries, types = self.__StdRegProv.EnumValues(
            hDefKey=regkey.h_key,
            sSubKeyName=regkey.sub_key
        )

        if not self.__format_return_code(rcode):
            raise EntryNotFound(
                "Nenhum registro encontrado para: {} em {}".format(regkey.sub_key, regkey.h_key)
            )

        return list(zip(entries, types))

    def delete_reg_value(self, regkey: RegKey, regvalue: RegValue) -> bool:

        if not self.__check_reg_access(regkey):
            raise NoAccessInKeyOrRegister(
                "Acesso Negado ou Chave inválida para o registro: {} em {}".format(
                    regkey.sub_key,
                    regkey.get_hkey_name(regkey.h_key)
                )
            )

        rcode, = self.__StdRegProv.DeleteValue(
            hDefKey=regkey.h_key,
            sSubKeyName=regkey.sub_key,
            sValueName=regvalue.reg_name
        )

        return self.__format_return_code(rcode)

    def get_reg_value(self, regkey: RegKey, value_name: str, value_type: int) -> RegValue:
        """
        Método para coletar o valor de uma entrada de um registro

        :param regkey:          Uma instância de RegKey
        :param value_name:      O nome da entrada do sub registro
        :param value_type:      O tipo do valor da chave de registro informada em value_name
        :return:                Mixed valor da chave com o tipo str ou int
        """

        function_get = None

        if value_type == RegValue.REG_SZ:
            function_get = self.__get_reg_string()
        elif value_type == RegValue.REG_DWORD:
            function_get = self.__get_reg_dword()
        elif value_type == RegValue.REG_QWORD:
            function_get = self.__get_reg_qword()
        elif value_type == RegValue.REG_EXPAND_SZ:
            function_get = self.__get_reg_expanded_string()
        elif value_type == RegValue.REG_BINARY:
            function_get = self.__get_reg_binary()
        elif value_type == RegValue.REG_MULTI_SZ:
            function_get = self.__get_reg_multiple_string()

        if function_get is not None:
            rcode, value = function_get(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=value_name
            )

            if self.__format_return_code(rcode):
                return RegValue(
                    reg_name=value_name,
                    reg_value=value,
                    reg_type=value_type
                )

    def set_reg_value(self, regkey: RegKey, reg_value: RegValue) -> bool:
        """
        Método para configurar um valor de registro

        :param regkey: Uma instancia de RegKey
        :param reg_value: Uma instancia de RegValue
        :return: bool
        """

        rcode = None

        if reg_value.reg_type == RegValue.REG_SZ:
            function_set = self.__set_reg_string()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                sValue=reg_value.reg_value
            )
        elif reg_value.reg_type == RegValue.REG_DWORD:
            function_set = self.__set_reg_dword()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                uValue=reg_value.reg_value
            )
        elif reg_value.reg_type == RegValue.REG_QWORD:
            function_set = self.__set_reg_qword()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                uValue=reg_value.reg_value
            )
        elif reg_value.reg_type == RegValue.REG_EXPAND_SZ:
            function_set = self.__set_reg_expanded_string()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                sValue=reg_value.reg_value
            )
        elif reg_value.reg_type == RegValue.REG_BINARY:
            function_set = self.__set_reg_binary()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                uValue=reg_value.reg_value
            )
        elif reg_value.reg_type == RegValue.REG_MULTI_SZ:
            function_set = self.__set_reg_multiple_string()
            rcode, = function_set(
                hDefKey=regkey.h_key,
                sSubKeyName=regkey.sub_key,
                sValueName=reg_value.reg_name,
                sValue=reg_value.reg_value
            )

        return self.__format_return_code(rcode)

    def __get_reg_string(self):
        """
        Retorna o lambda do método GetStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetStringValue

    def __get_reg_expanded_string(self):
        """
        Retorna o lambda do método GetExpandedStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetExpandedStringValue

    def __get_reg_binary(self):
        """
        Retorna o lambda do método GetBinaryValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetBinaryValue

    def __get_reg_dword(self):
        """
        Retorna o lambda do método GetDWORDValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetDWORDValue

    def __get_reg_qword(self):
        """
        Retorna o lambda do método GetQWORDValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetQWORDValue

    def __get_reg_multiple_string(self):
        """
        Retorna o lambda do método GetMultiStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.GetMultiStringValue

    def __set_reg_string(self):
        """
        Retorna o lambda do método SetStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.SetStringValue

    def __set_reg_expanded_string(self):
        """
        Retorna o lambda do método SetExpandedStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.SetExpandedStringValue

    def __set_reg_binary(self):
        """
        Retorna o lambda do método SetBinaryValue da classe StdRegProv
        :return: function
        """

        return self.__StdRegProv.SetBinaryValue

    def __set_reg_dword(self):
        """
        Retorna o lambda do método SetDWORDValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.SetDWORDValue

    def __set_reg_qword(self):
        """
        Retorna o lambda do método SetQWORDValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.SetQWORDValue

    def __set_reg_multiple_string(self):
        """
        Retorna o lambda do método SetMultiStringValue da classe StdRegProv
        :return: function
        """
        return self.__StdRegProv.SetMultiStringValue

    def __format_return_code(self, return_code: int) -> bool:
        rc = win32api.FormatMessage(return_code)
        if return_code > 0:
            raise OSError(rc)
        return True

    def __string_to_reg_expanded(self, string: str) -> str:
        """
        Converte uma string de valor real ASCII para o padrão de
        REG_EXPAND_SZ

        Converta cada caractere em seu equivalente hexadecimal e,
        em seguida, insira o valor como uma série de números
        hexadecimais de dois dígitos, separando cada número
        com uma vírgula e separando cada caractere com 00.

        ref: http://www.informit.com/articles/article.aspx?p=1378466&seqNum=4

        :param string:  Uma string com caracteres de ASCII
                        e.g.: C:\\Users\\Matias

        :return:        Retorna uma string com o valor formatado
                        para REG_EXPAND_SZ.
                        E.g.: 'hex(2):43,00,3a,00,5c,00,55,00,73,00,\
                        65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,\
                        00,61,00,73,00'
        """
        hex_string = ",00,".join("{:02x}".format(ord(c)) for c in string)
        return "hex(2):{start}{end}".format(start=hex_string, end=",00")

    def __reg_expanded_to_string(self, string: str) -> str:
        """
        Converte uma string do tipo REG_EXPAND_SZ para o valor real.
        Faz o oposto de __string_to_reg_expanded
        ref: http://www.informit.com/articles/article.aspx?p=1378466&seqNum=4

        :param string:      A string com o formato de um REG_EXPAND_SZ
                            e.g: 'hex(2):43,00,3a,00,5c,00,55,00,73,00,\
                            65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,\
                            00,61,00,73,00'
        :return:            Uma string formatada para o valor da tabela ASCII
                            e.g: C:\\Users\\Matias
        """
        hex_chars = string.strip("hex(2):").rstrip(",00").split(",00,")
        number_ascii = (int(number_ascii, 16) for number_ascii in hex_chars)
        string_output = "".join("{}".format(chr(chars)) for chars in number_ascii)
        return string_output

    def __int_to_reg_dword(self, string_binary: int) -> str:
        """
        Retorna um valor DWORD de oito dígitos em hexadecimal
        ref: http://www.informit.com/articles/article.aspx?p=1378466&seqNum=4
        :param string_binary: um inteiro
        :return: Uma string com 8 dígitos de um hexadecimal
        """
        if type(string_binary) is str or type(string_binary) is int:
            integer_binary = int(string_binary)

            if integer_binary < 0:
                integer_binary = self.__convert_int_signed_to_unsigned(integer_binary)

            output = "dword:{0:08x}".format(integer_binary)

            if len(output) > 8 + 6:
                raise MaxLength("A quantidade dos dígitos excede a 8")

            return output

        raise TypeError("O valor deve ser str ou int")

    def __convert_int_signed_to_unsigned(self, integer_unsigned: int) -> int:
        """
        Converte um inteiro negativo para positivo somando o número a
        2^32

        :param integer_signed:  Um inteiro negativo
        :return:                Um inteiro positivo
        """
        if integer_unsigned >= 0:
            raise ValueError("O número inteiro deve estar negativo para remover a assinatura")

        return integer_unsigned + (2 ** 32)

    def __convert_int_unsigned_to_signed(self, integer_signed: int) -> int:
        """
        Converte um inteiro positivo para negativo subtraindo o número a
        2^32

        :param integer_signed:      Um inteiro positivo
        :return:
        """
        if integer_signed < 0:
            raise ValueError("O número inteiro deve ser positivo para adicionar a assinatura negativa")

        return integer_signed - (2**32)

    def __reg_dword_to_int(self, str_dword: str) -> int:
        """
        Reverte a string com 8 dígitos + 6 dígitos do nome ´dword:´
        de um registro REG_DWORD em hexadecimal para um inteiro

        :param str_dword:   Uma string com um interiro de 8 dígitos
        :return:            Um número inteiro
        """
        if type(str_dword) is not str or len(str_dword) > 8 + 6:
            raise TypeError("O valor deve ser str com o formato REG_DWORD")

        return int(str_dword.strip('dword:'), 16)

    def __list_integer_to_reg_binary_hex(self, binaries: List[int]) -> str:
        """
        Converte um valor binário em uma série de números
        hexadecimais de dois dígitos, separando cada número
        com uma vírgula.

        :param binaries: Um valor binário
        :return:    string hex correspondente ao registro REG_BINARY
        """
        if type(binaries) != list:
            raise TypeError("O valor deve ser um iteravel com valores binários")

        if type(binaries) is list:
            for value_type in binaries:
                if type(value_type) != int:
                    raise TypeError("Os valores internos da lista deve ser um int")

        hex_string = ",".join("{:02x}".format(int_number) for int_number in binaries)

        return "hex:{}".format(hex_string)

    def __reg_binary_hex_to_list_integer(self, binary_reg: str) -> List:
        """
        Realiza o oposto de __binary_to_hex
        :param binary_reg:      Uma string com o valor de REG_BINARY
        :return:                List com str
        """
        if type(binary_reg) != str:
            raise TypeError("O valor do registro REG_BINARY esta incorreto")

        hex_chars = binary_reg.strip("hex:").rstrip(",").split(",")

        if len(hex_chars) > 0 and hex_chars[0] != '':
            return [int(number_ascii, 16) for number_ascii in hex_chars]
        return []

    def __list_string_to_reg_multi_string(self, list_string: List[str]) -> str:
        """
        Formata uma lista de strings para o registro REG_MULT_SZ

        Um registro do tipo REG_MULT_SZ deve ser uma string com o caracter
        \0

        Converte cada caractere em seu equivalente hexadecimal e,
        em seguida, inclui o valor como uma série de números
        hexadecimais de dois dígitos, separando cada número com
        uma vírgula e separando cada caractere com 00 e separando
        cada sequência com espaço (hex 00).
        E.g: hex(7):43,00,3a,00,5c,00,55,00,73,00,65,00,72,00,73,
        00,00,00,00,00,  44,00,3a,00,5c,00,52,00,65,00,63,00,6f,00,
        72,00,64,00,00,00,00,00


        ref: http://www.informit.com/articles/article.aspx?p=1378466&seqNum=4

        :param list_string:     Uma lista com as strings para o registro
        :return:                str formatada no padrão hex(7) para REG_MULTI_SZ

        """

        if type(list_string) != list:
            raise TypeError("O tipo de valor deve ser uma lista")

        if type(list_string) == list:
            for value_type in list_string:
                if type(value_type) != str:
                    raise TypeError("O valor dos itens da lista devem ser str")
        hex_strings = []
        for string in list_string:
            hex_strings.append(",00,".join("{:02x}".format(ord(c)) for c in string))

        hex_string_joined = "  ".join(
            "{},00,00,00,00,00,".format(hex_string) for hex_string in [c for c in hex_strings]
        )

        return "hex(7):{hex_strings}".format(hex_strings=hex_string_joined).rstrip(',')

    def __reg_multi_string_to_list_string(self, reg_multi_string: str) -> List[str]:
        """
        Recebe uma string formatada para o valor de um registro
        REG_MULTI_SZ e retorna uma lista com os valores reais.

        E.g: hex(7):43,00,3a,00,5c,00,55,00,73,00,65,00,72,00,73,
        00,00,00,00,00,  44,00,3a,00,5c,00,52,00,65,00,63,00,6f,00,
        72,00,64,00,00,00,00,00'


        :param reg_multi_string:    A string no formato hex(7) de REG_MULTI_SZ
        :return:                    List com os valores reais
        """

        if type(reg_multi_string) != str:
            raise TypeError("O tipo do valor não é uma str")

        if 'hex(7):' not in reg_multi_string:
            raise ValueError("O tipo de string não corresponde ao formato hex(7) de REG_MULTI_SZ")

        list_hex_strings = reg_multi_string.strip("hex(7):").split("  ")

        # Matriz com os caracteres em hex com dois dígitos [['43'], ['44']]
        list_hex_chars = [hex_strings.strip(',00,00,00,00,00,').split(',00,') for hex_strings in list_hex_strings]

        list_string_output = []
        for hex_chars in list_hex_chars:
            number_ascii = (int(number_ascii, 16) for number_ascii in hex_chars)
            list_string_output.append("".join("{}".format(chr(chars)) for chars in number_ascii))

        return list_string_output

    def __string_to_reg_qword(self, string: str) -> str:
        """
        Método para formatar uma string para o padrão REG_QWORD
        Insere oito pares hexadecimais de dois dígitos, separados por
        vírgulas, com os pares sendo executados da ordem mais alta para
        a mais baixa

        :param string:      Uma string com 16 caracteres aceitos no padrão
                            hexadecimal
        :return:            string formatada no formato de REG_QWORD
        """
        if type(string) != str:
            raise TypeError('O tipo de valor deve ser str')

        if len(string) > 16:
            raise MaxLength('A quantidade de dígitos ultrapassa a base 16 do hexadecimal')

        chars = [char for char in string if not char.isdigit()]
        for char in chars:
            if char not in 'abcdef':
                raise TypeError('Os caracteres alpha não correspondem ao utilizado em hexadecimal')

        if len(string) < 16:
            string = format(string, '0>16')

        limitator = 2
        string_split = [string[counter:counter + limitator] for counter in range(0, len(string), limitator)]
        string_split.reverse()

        string_joined = ",".join("{}".format(units) for units in string_split)

        return "hex(b):{string_fliped}".format(string_fliped=string_joined)

    def __reg_qword_to_string(self, reg_qword_string: str) -> str:
        """
        Método para reverter um valor do registro REG_QWORD

        :param reg_qword_string:    Uma string no formato REG_QWORD
        :return:                    String com o valor original do registro
        """

        if type(reg_qword_string) != str or 'hex(b):' not in reg_qword_string:
            raise ValueError("O parametro passado não é uma str válida")

        reg_qword_string = reg_qword_string.strip("hex(b):")

        list_double_chars = [double_chars for double_chars in reg_qword_string.strip(',00').split(',')]
        list_double_chars.reverse()
        return "".join(double_chars for double_chars in list_double_chars)

    def __string_to_reg_sz(self, string: str) -> str:
        """
        Converte uma string para o formato de REG_SZ
        :param string:      Uma string comum
        :return:            Uma string com aspas duplas escapadas
        """
        if type(string) is not str:
            raise TypeError('O tipo de valor para um REG_SZ deve ser str')

        return "\"{}\"".format(string)

    def __reg_sz_to_string(self, string: str) -> str:
        """
        Converte um valor de registro REG_SZ para string sem aspas duplas
        :param string:      Uma string formatada para um registro
        :return:            String sem aspas duplas
        """
        if type(string) is not str:
            raise TypeError('O tipo de valor de um REG_SZ deve ser str com \" \"')

        return string.strip('\"')

    def convert_value_to_raw(self, value: object, value_type: int) -> str:
        """
        Converte um valor real para o valor gravado no registro

        :param value:       O valor correspondente ao tipo do valor de um registro
        :param value_type:  O tipo de valor do registro
        :return:            String formatada
        """
        output_str = None

        if value_type == RegValue.REG_SZ:
            output_str = self.__string_to_reg_sz(value)

        elif value_type == RegValue.REG_DWORD:
            output_str = self.__int_to_reg_dword(value)

        elif value_type == RegValue.REG_QWORD:
            output_str = self.__string_to_reg_qword(value)

        elif value_type == RegValue.REG_EXPAND_SZ:
            output_str = self.__string_to_reg_expanded(value)

        elif value_type == RegValue.REG_BINARY:
            output_str = self.__list_integer_to_reg_binary_hex(value)

        elif value_type == RegValue.REG_MULTI_SZ:
            output_str = self.__list_string_to_reg_multi_string(value)

        return output_str

    def convert_raw_to_value(self, raw: object, value_type: int) -> object:
        """
        Converte um valor raw gradado em um registro para o valor real

        :param raw:       O valor correspondente ao tipo do valor de um registro
        :param value_type:  O tipo de valor do registro
        :return:            String formatada
        """
        output_str = None

        if value_type == RegValue.REG_SZ:
            output_str = self.__reg_sz_to_string(raw)

        elif value_type == RegValue.REG_DWORD:
            output_str = self.__reg_dword_to_int(raw)

        elif value_type == RegValue.REG_QWORD:
            output_str = self.__reg_qword_to_string(raw)

        elif value_type == RegValue.REG_EXPAND_SZ:
            output_str = self.__reg_expanded_to_string(raw)

        elif value_type == RegValue.REG_BINARY:
            output_str = self.__reg_binary_hex_to_list_integer(raw)

        elif value_type == RegValue.REG_MULTI_SZ:
            output_str = self.__reg_multi_string_to_list_string(raw)

        return output_str

    def get_registries(self, regkey: RegKey) -> List[Dict]:
        """
        Converte os valores de uma chave e suas subchaves em uma lista
        com dicionário de registros.
        Ex.:
        [
            {
                'reg_path': '[Chave]',
                'reg_values': [(NomeDoValor, Valor, TipoDoValor),(NomeDoValor, Valor, TipoDoValor)]
            },
            {
                'reg_path': '[Chave\\Subchave]',
                'reg_values': [(NomeDoValor, Valor, TipoDoValor),(NomeDoValor, Valor, TipoDoValor)]
            }
        ]
        :param regkey:
        :return:
        """

        list_of_registries = []

        # Coleta os valores da chave principal informada
        reg_values = self.get_value_name(regkey)

        if len(reg_values) > 0:
            values = []

            for reg_value_name, reg_value_type in reg_values:
                regvalue = self.get_reg_value(regkey, reg_value_name, reg_value_type)

                values.append((
                    regvalue.reg_name,
                    regvalue.reg_value,
                    regvalue.reg_type,
                ))

            """
            Cria o dicionario com os dados do registro da chave informada
            ex.: 
            {
                'reg_path': '[Chave]',
                'reg_values': [(NomeDoValor, Valor, TipoDoValor),(NomeDoValor, Valor, TipoDoValor)]
            }
            """
            reg_data = {
                'reg_path': "[{hkey}\\{subkey}]".format(
                    hkey=regkey.get_hkey_name(regkey.h_key),
                    subkey=regkey.sub_key
                ),
                'reg_values': values
            }

            # Adiciona o dicionario na lista de registros
            list_of_registries.append(reg_data)

        # Coleta as subchaves da chave informada
        sub_regkeys = self.get_keys_names(regkey)

        if len(sub_regkeys) > 0:

            # Iteração das subchaves
            for sub_regkey in sub_regkeys:
                # Coleta os nomes dos valores das subchaves
                sub_reg_values = self.get_value_name(sub_regkey)

                sub_values = []

                if len(sub_reg_values) > 0:

                    # Iteração dos sub_reg_values
                    for sub_reg_value_name, sub_reg_value_type in sub_reg_values:
                        # Coleta os dados dos valores das subchaves e
                        # transforma em uma instancia de RegValue para
                        # depois inclui-los na lista de valores `sub_values`
                        sub_regvalue = self.get_reg_value(sub_regkey, sub_reg_value_name, sub_reg_value_type)
                        sub_values.append((
                            sub_regvalue.reg_name,
                            sub_regvalue.reg_value,
                            sub_regvalue.reg_type,
                        ))

                # Cria o dicionario com o nome da subchave e com seus valores
                sub_reg_data = {
                    'reg_path': "[{hkey}\\{subkey}]".format(
                        hkey=sub_regkey.get_hkey_name(sub_regkey.h_key),
                        subkey=sub_regkey.sub_key
                    ),
                    'reg_values': sub_values
                }

                # Adiciona na lista de registro o dicionario da sub chaves
                list_of_registries.append(sub_reg_data)

        return list_of_registries

    def export_reg(self, regkey: RegKey, file_path: str) -> bool:
        """
        Exporta os registros da chave regkey e suas subchaves para
        um arquivo em file_path

        :param regkey:          Uma instancia de RegKey com h_key e subkey
        :param file_path:       Um caminho para um arquivo com nome e extenção .reg
        :return:
        """
        if not isinstance(regkey, RegKey) or not isinstance(file_path, str):
            raise TypeError(
                'O valor do parâmetro não é uma instancia de RegKey ou o caminho do arquivo não é valido'
            )

        if not os.path.isdir(os.path.dirname(file_path)):
            raise NotADirectoryError(
                'O caminho informado não é um diretório válido'
            )

        if os.path.exists(file_path):
            os.unlink(file_path)

        # Cria um novo arquivo
        file_handle = open(file_path, 'w')

        try:
            with file_handle as file:
                # Cabeçalho com a versão do editor de registro do Win32
                file.write("Windows Registry Editor Version 5.00\n\n")

                # Iteração para gravar os dados do registro no arquivo
                for registry in self.get_registries(regkey):
                    file.write("{regpath}\n".format(regpath=registry['reg_path']))

                    # Iteração para gravar os valores relacionado a chave
                    for value_name, value, value_type in registry['reg_values']:

                        # Convertendo o valor real para o aceito no arquivo de reg
                        registry_value = self.convert_value_to_raw(value, value_type)

                        # Escrevendo os valores da chave com o par "NomeDoValor"=ValorConvertido
                        file.write("\"{regvalue_name}\"=".format(regvalue_name=value_name))
                        file.write("{regvalue}\n".format(regvalue=registry_value))
                    file.write("\n")

                return True
        except Exception:
            raise ExportError("Falha ao exportar o arquivo de registro para {}.".format(file_path))

    def __identify_value_type(self, value: str) -> int:
        """
        Método para identificar o tipo de dados apartir de sua composição
        :param value:   String com o dado com a estrutura do valor
        :return:        Um inteiro RegValue.REG_... com o tipo de valor
        """
        if type(value) is not str:
            raise TypeError(
                'O tipo de valor deve ser uma string válida com o valor raw de um registro'
            )

        if 'dword:' in value:
            return RegValue.REG_DWORD
        elif 'hex(2):' in value:
            return RegValue.REG_EXPAND_SZ
        elif 'hex(7):' in value:
            return RegValue.REG_MULTI_SZ
        elif 'hex(b):' in value:
            return RegValue.REG_QWORD
        elif 'hex:' in value:
            return RegValue.REG_BINARY
        else:
            return RegValue.REG_SZ

    def load_registry(self, file_path: str) -> List[Dict]:
        """
        Método para converter um arquivo estruturado como registros do
        Windows para uma lista com Dicionários.

        Ex.:
        [
            {
                'reg_path': '[Chave]',
                'reg_value': (NomeDoValor, Valor, TipoDoValor)
            }
        ]
        :param file_path:   Um caminho para um arquivo .reg válido
        :return:            list
        :raises:            FileNotFoundError, NotADirectoryError
        """

        if not os.path.isdir(os.path.dirname(file_path)):
            raise NotADirectoryError(
                'O caminho informado não é um diretório válido'
            )

        if '.reg' not in os.path.basename(file_path):
            raise FileNotFoundError(
                'O arquivo informado não contém a extenção correta para um .reg'
            )

        regvalue, regkey = None, None
        header_registry = r"Windows Registry Editor Version 5.00"

        with open(file_path, 'r') as reg_file:
            if header_registry not in reg_file.readline():
                raise ErrorRegistyFormat(
                    'O arquivo não contém a formatação válida de um registro version 5.00 do windows'
                )

            hkey_regex = r"\[+HKEY_+.*\]"
            value_regex = r"\"(.*)\"+=+(.*)"
            registries = []

            for line in reg_file:

                # Coletando o HKEY_ e o subkey do registro
                key = re.findall(hkey_regex, line, re.MULTILINE)

                if len(key) > 0:
                    regkey = key[0]

                # Coletando o nome do valor, o tipo do valor e o valor
                value = re.findall(value_regex, line, re.MULTILINE)
                if len(value) > 0:
                    reg_value_name, reg_raw_value = value[0]
                    reg_value_type = self.__identify_value_type(reg_raw_value)

                    # Convertendo o valor raw do registro para o valor comum
                    reg_value = self.convert_raw_to_value(reg_raw_value, reg_value_type)

                    reg_data = {
                        'reg_path': regkey,
                        'reg_value': (reg_value_name, reg_value, reg_value_type)
                    }
                    registries.append(reg_data)
            return registries

    def import_registry(self, file_path: str) -> bool:
        """
        Método para importar registros para o sistema a partir
        de um arquivo .reg estruturado
        :param file_path:       Caminho absoluto para um arquivo .reg
        :return:                Bool True caso tenha sucesso na importação
        :raise RegImportError:  Lança uma exception RegImportError caso ocorra
                                algum erro no processo.
        """
        hkey_regex = r"\[+(HKEY_+\w*)+\\(.*)\]"

        try:
            for registry in self.load_registry(file_path):
                key = re.findall(hkey_regex, registry['reg_path'])
                if len(key) > 0:
                    h_key, sub_key = key[0]
                    regkey = RegKey(getattr(RegKey, h_key), sub_key)

                    if self.create_key(regkey):
                        reg_value_name, reg_value, reg_value_type = registry['reg_value']
                        regvalue = RegValue(reg_value_name, reg_value, reg_value_type)

                        self.set_reg_value(regkey, regvalue)
            return True
        except Exception:
            raise RegImportError(
                'Falha ao importar o arquivo de registro para o sistema.'
            )
