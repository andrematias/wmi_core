class NotValidWmiNamespaceInstance(Exception):
    pass


class NoAccessInKeyOrRegister(Exception):
    pass


class EntryNotFound(Exception):
    pass


class MaxLength(Exception):
    pass


class ExportError(Exception):
    pass


class RegImportError(Exception):
    pass


class ErrorRegistyFormat(Exception):
    pass