class NotValidWmiNamespaceInstance(Exception):
    pass


class ReadProcessError(Exception):
    pass


class InitProcessError(Exception):
    pass


class WatchProcessError(Exception):
    pass
