# -*- coding: UTF-8 -*-
import re
import wmi
import logging

logger = logging.getLogger(__name__)

class WmiConnect(object):

    @classmethod
    def connect(
            cls,
            attemps=3,
            computer="",
            impersonation_level="",
            authentication_level="",
            authority="",
            privileges="",
            moniker="",
            _wmi=None,
            namespace="",
            suffix="",
            user="",
            password="",
            find_classes=False,
            debug=False
    ):
        global attemp
        attemp = 0

        while attemp < attemps:

            logger.info('Tentativa de conexão remota ao computador %s Nº: %d' % (computer, (attemp + 1)))
            
            try:
                if attemp == attemps:
                    raise Exception('Falha na conexão com o servidor %s Tentativas de conexão encerradas.' % computer)

                _wmi_namespace_remote = wmi.WMI(
                    computer=computer,
                    impersonation_level=impersonation_level,
                    authentication_level=authentication_level,
                    authority=authority,
                    privileges=privileges,
                    moniker=moniker,
                    wmi=_wmi,
                    namespace=namespace,
                    suffix=suffix,
                    user=user,
                    password=password,
                    find_classes=find_classes,
                    debug=debug
                )
                return _wmi_namespace_remote
            except wmi.x_wmi as ex:
                regex = r"RPC"
                find_rpc_fail = re.findall(regex, str(ex.com_error))

                regex_access_error = r"Acesso negado"
                find_access_error = re.findall(regex_access_error, str(ex.com_error))

                if len(find_rpc_fail) > 0:
                    logger.warning(f'O RPC do servidor {computer} não esta respondendo no momento.')
                    attemp += 1
                elif len(find_access_error) > 0:
                    logger.warning('WMI: Acesso Negado')
                    raise WmiExceptionConnection('WMI: Acesso Negado')

                else:
                    logger.warning(ex.com_error)
                    raise WmiExceptionConnection(ex.com_error)

            except Exception as ex:
                logger.error(ex)
                raise UnexpectedExceptionWmiConnection(ex)


class UnexpectedExceptionWmiConnection(Exception):
    pass


class WmiExceptionConnection(Exception):
    pass
