# -*- coding: UTF-8 -*-
from unittest import TestCase, main
import wmi
from ..metadata.MetaData import MetaData
from ..wmi_connector.connector import WmiConnect


class TestMetaData(TestCase):

    def setUp(self):
        self._wmi_namespace = wmi.WMI()

        self._wmi_namespace_remote = WmiConnect.connect(
            computer=r"10.228.1.144",
            password=None,
            user=r"lmcorp\andre.matias"
        )

        self.__metadata_remote = MetaData(self._wmi_namespace_remote)
        self.__metadata = MetaData(self._wmi_namespace)

    def test_list_method_remotly(self):
        actual = self.__metadata_remote.list(Extension="dll",
                                             Path="\\bkpspskyur80\\20181206_22h49min_parcial\\ura\\lmcti\\app\\")
        self.assertIsInstance(actual, list)
        if len(actual) > 0:
            self.assertIsInstance(actual[0], wmi._wmi_object)

        self.assertRaises(
            ValueError,
            self.__metadata_remote.list,
            None
        )

    def test_to_dict_method_remotly(self):
        metadata_list = self.__metadata_remote.list(Extension="dll",
                                                    Path="\\bkpspskyur80\\20181206_22h49min_parcial\\ura\\lmcti\\app\\")
        actual = self.__metadata_remote.to_dict(metadata_list)
        self.assertIsInstance(actual, list)

        if len(actual) > 0:
            self.assertIsInstance(actual[0], dict)

    def test_read_file_remotly(self):
        """
        Carrega um arquivo para ser lido e coleta meta informações,
        retorna um dicionário com os metadados do arquivo
        """
        actual = self.__metadata_remote.read_file('C:\\lmcti\\App\\URASKY.dll')
        self.assertIsInstance(actual, dict)
        if len(actual) > 0:
            self.assertTrue(int(actual['FileSize']) > 0)

        self.assertRaises(
            ValueError,
            self.__metadata_remote.read_file,
            None
        )


if __name__ is "__main__":
    main()
