# -*- coding: UTF-8 -*-
from unittest import TestCase, main
from ..control.registries.Registry import RegValue


class TestRegValue(TestCase):
    def test_if_registry_constants_exists(self):
        """
            Test if Constants of the registries types
            exists in Registry class
        :return:
        """
        constants_names = (
            'REG_SZ',
            'REG_EXPAND_SZ',
            'REG_BINARY',
            'REG_DWORD',
            'REG_MULTI_SZ',
            'REG_QWORD'
        )

        for constant in constants_names:
            self.assertTrue(constant in dir(RegValue))


if __name__ is "__main__":
    main()
