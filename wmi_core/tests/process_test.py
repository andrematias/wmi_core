# -*- coding: UTF-8 -*-
from unittest import TestCase, main
from ..control.processes.Process import Process
import wmi


class TestProcess(TestCase):

    USER_NAME = r"lmcorp\andre.matias"
    USER_PASSWORD = None
    REMOTE_COMPUTER = '10.228.1.144'
    COMPUTER = r'localhost'
    NAMESPACE = r'root/cimv2'

    # Atualizar com um process id atual para o teste
    PROCESS_ID = 688
    REMOTE_PROCESS_ID = 1888

    def setUp(self):
        """
        Set variables and instances of class on test target
        :return:
        """
        self._wmi_namespace = wmi.WMI()

        self._wmi_namespace_remote = wmi.WMI(
            computer=self.REMOTE_COMPUTER,
            user=self.USER_NAME,
            password=self.USER_PASSWORD,
            namespace=self.NAMESPACE
        )

        self.__process = Process(self._wmi_namespace)
        self.__process_remote = Process(self._wmi_namespace_remote)

    def test_list_process(self):
        actual = self.__process.list_process()
        self.assertIsInstance(actual, list)

    def test_list_process_remotly(self):
        actual = self.__process_remote.list_process()
        self.assertIsInstance(actual, list)

    def test_get_process_by_id(self):
        actual = self.__process.get_process_by_id(self.PROCESS_ID)
        self.assertIsInstance(actual, dict)

        self.assertRaises(
            TypeError,
            self.__process.get_process_by_id,
            str(self.PROCESS_ID)
        )

    def test_get_process_like_name(self):
        actual = self.__process.get_process_like_name('notepad')
        self.assertIsInstance(actual, list)

    def test_init_process(self):
        cmd = "notepad.exe"
        actual = self.__process.init_process(cmd)
        self.assertIsInstance(actual, dict)

    def test_init_process_remotly(self):
        cmd = "notepad.exe"
        actual = self.__process_remote.init_process(cmd)
        self.assertIsInstance(actual, dict)

    def test_stop_process(self):
        cmd = "notepad.exe"
        process = self.__process.init_process(cmd)
        actual = self.__process.stop_process_by_id(process['ProcessId'])
        self.assertTrue(actual)

    def test_stop_all_process_like_name(self):
        cmd = "notepad.exe"
        process = self.__process.init_process(cmd)
        actual = self.__process.stop_all_process_like_name(process['Caption'])
        self.assertTrue(actual)

    def test_stop_all_process_like_name_remotly(self):
        cmd = "notepad.exe"
        process = self.__process.init_process(cmd)
        actual = self.__process_remote.stop_all_process_like_name(process['Caption'])
        self.assertTrue(actual)

    def test_watch_process(self):
        cmd = "calc.exe"
        process = self.__process.init_process(cmd)
        actual = self.__process.watch_process(process['Caption'], 'operation')

        self.__process.stop_all_process_like_name(process['Caption'])
        self.assertIsInstance(actual, dict)

    def test_get_owner_process_by_id(self):
        cmd = "calc.exe"
        process = self.__process.init_process(cmd)
        actual = self.__process.get_owner_process_by_id(process['ProcessId'])

        self.__process.stop_all_process_like_name(process['Caption'])
        self.assertIsInstance(actual, dict)

    def test_dict(self):
        actual = self.__process.properties
        self.assertIsInstance(actual, list)


if __name__ == '__main__':
    main()
