# -*- coding: UTF-8 -*-
from unittest import TestCase, main
import re
import wmi
from ..control.services.Services import (
    ServiceManager, Service
)
from wmi_core.control.services.ServicesExceptions import *


def conn_remote(attemps=3):
    user_name = r"lmcorp\andre.matias"
    user_password = None
    remote_computer = '10.228.1.144'
    namespace = r'root/cimv2'

    global attemp
    attemp = 0
    while attemp < attemps:
        print('Tentativa de conexão remota Nº: %d' % (attemp + 1))

        if attemp == 3:
            raise Exception('Falha inesperada na conexão com o servidor remoto')

        try:
            _wmi_namespace_remote = wmi.WMI(
                computer=remote_computer,
                user=user_name,
                password=user_password,
                namespace=namespace
            )

            return _wmi_namespace_remote
        except wmi.x_wmi as ex:
            regex = r"RPC"
            find = re.findall(regex, str(ex.com_error))
            if len(find) > 0:
                attemp += 1
        except Exception as ex:
            raise Exception(ex)


class TestServices(TestCase):

    def setUp(self):
        self._wmi_namespace = wmi.WMI()

        self._wmi_namespace_remote = conn_remote()

        self.__service_remote = Service(self._wmi_namespace_remote)
        self.__service = Service(self._wmi_namespace)

        # Configurações para serviços correntes na maquina
        # Alterar para um serviço válido
        self.service_name = "TlntSvr"
        self.service_name_remote = "LanmanServer"

    def test_service_list(self):
        self.assertRaises(
            LoadServicesError,
            self.__service.list,
            Named='NotValid'
        )

        actual = self.__service.list(Name=self.service_name)
        self.assertIsInstance(actual, list)

    def test_win32_service_to_dict(self):
        """
        Converte uma lista de Win32_Service em uma lista
        com dicionarios com as propriedades/valor
        """
        win32_services = self.__service_remote.list(Name=self.service_name_remote)

        actual = self.__service_remote.to_dict(win32_services)
        self.assertListEqual(list(actual[0]), self.__service_remote.properties)

    def test_start_services_remotly(self):
        """
        Testa se o método start_sevice inicia um serviço existente
        no sistema e retorna um bool caso contrario uma StateServicesError
        é lançada
        """
        win32_services = self.__service_remote.list(Name=self.service_name_remote)

        for service in win32_services:
            service_manager = ServiceManager(service)
            self.assertTrue(service_manager.start_service())

    def test_stop_services_remotly(self):
        """
        Testa se o método stop_sevices para um serviço existente
        no sistema e retorna um bool caso contrario uma StopServicesError
        é lançada
        """
        win32_services = self.__service_remote.list(Name=self.service_name_remote)

        for service in win32_services:
            service_manager = ServiceManager(service)
            self.assertTrue(service_manager.stop_service())

    def test_pause_service_remotly(self):
        """
        Testa o método pause_service para um serviço existente
        no sistema e retorna um bool caso contrario uma StateServicesError
        é lançada
        """
        win32_services = self.__service_remote.list(Name=self.service_name_remote)

        for service in win32_services:
            service_manager = ServiceManager(service)
            service_manager.start_service()

            service_manager = ServiceManager(self.__service_remote.load(service))
            self.assertTrue(service_manager.pause_service())

    def test_resume_service_remotly(self):
        """
        Testa o método pause_service para um serviço existente
        no sistema e retorna um bool caso contrario uma StateServicesError
        é lançada
        """
        win32_services = self.__service_remote.list(Name=self.service_name_remote)

        for service in win32_services:
            service_manager = ServiceManager(service)
            service_manager.start_service()

            service_manager = ServiceManager(self.__service_remote.load(service))
            self.assertTrue(service_manager.resume_service())


if __name__ is "__main__":
    main()
