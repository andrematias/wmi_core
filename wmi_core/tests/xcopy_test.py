# -*- coding: UTF-8 -*-
import os
import sys
import tempfile
from unittest import TestCase, main

from ..copier.copy import XCopy


class TestNsxXCopy(TestCase):

    def setUp(self):
        """
            Configuração do teste
        """
        self._tmp_fileTest = tempfile.mkstemp()[-1]
        self._directory = "C:\\Users\\Matias\\Documents\\Notes"
        self._directory_tmp = "C:\\Users\\Matias\\AppData\\Local\\Temp"
        self._xcopy_instance = XCopy()
        self._wildcard = '*.bmp'

    def test_if_import_necessaries_modules(self):
        """
            Teste para saber se os módulos necessários foram carregados
            no ambiente em sys.modules
        """
        modules_names = ['shutil', 'os', 'glob']
        for module in modules_names:
            self.assertTrue(module in sys.modules)

    def test_if_class_XCopy_exists(self):
        """
            Teste para verificar se a classe XCopy existe
            Testa se a instancia do objeto `_xcopy_instance` é do
            tipo XCopy
        """
        self.assertIsInstance(self._xcopy_instance, XCopy, "Este objeto não é uma instancia de NsxXCopy")

    def test_private_method_file_exists(self):
        """
            Teste para verificar o método privado
            __file_exists
        """
        self.assertTrue(self._xcopy_instance._XCopy__file_exists(self._tmp_fileTest))

    def test_if_method_copy_file_exists(self):
        """
            Teste para verificar se o método copy_file
            existe na classe XCopy
        """
        self.assertTrue('copy_file' in dir(XCopy))

    def test_if_copy_file_methos_throw_FileNotFoundError(self):
        """
            Teste para verificar se o método copy_file
            lança uma Exception FileNotFoundError se
            o arquivo em `file_path` for inválido
        """
        self.assertRaises(
            FileNotFoundError,
            self._xcopy_instance.copy_file,
            'not_valid_file',
            r"{}Copia do arquivo.txt".format(self._directory_tmp)
        )

    def test_if_copy_a_single_file(self):
        """
            Teste para verificar se o método copy_file
            faz a cópia de um arquivo para outro local
            com um novo nome, somente com o diretório
            de destino ou sobrescrever um arquivo
        """

        # Quando for específicado o caminho completo contendo o nome
        # do novo arquivo
        self.assertTrue(
            self._xcopy_instance.copy_file(self._tmp_fileTest, "{}\\Copia do arquivo.txt".format(self._directory_tmp))
        )

        # Caso seja especificado somente o diretório de destino, sem informar
        # o nome do arquivo
        self.assertTrue(
            self._xcopy_instance.copy_file(self._tmp_fileTest, "{}\\new_tmp".format(self._directory_tmp))
        )

    def test_get_files_by_wildcard(self):
        self.assertTrue(
            len(
                self._xcopy_instance._XCopy__get_files_by_wildcard(
                    "{directory}/{wildcard}".format(
                        directory=self._directory,
                        wildcard=self._wildcard
                    )
                )
            ) > 0
        )

    def test_if_copy_files_by_wildcard(self):
        self.assertTrue(
            self._xcopy_instance.copy_file(
                "{directory}/{wildcard}".format(
                    directory=self._directory,
                    wildcard=self._wildcard
                ),
                self._directory_tmp
            )
        )

    def test_if_not_rewrite_file_if_file_exists_in_destinity(self):
        """
            Teste para verificar o cancelamento de
            sobrescrição de arquivo
        """
        # Caso seja solicitado para sobrescrever o arquivo no diretório de destino
        # deve lançar um FileExistsError
        self.assertRaises(
            FileExistsError,
            self._xcopy_instance.copy_file,
            self._tmp_fileTest,
            "{}\\Copia do arquivo.txt".format(self._directory_tmp),
            False
        )

    def test_if_copy_tree_method_overwrite(self):
        """
            Teste para verificar a cópia de diretórios
            recursivo sobrescrevendo o destinity
        """
        self.assertTrue(
            self._xcopy_instance.copy_tree(
                source = self._directory,
                destinity = "{}\\Copia".format(self._directory_tmp),
                overwrite = True
            )
        )

    def test_if_copy_tree_method_ignore_patters(self):
        """
            Teste para verificar a cópia de um diretório
            recusivamente ignorando alguns padrões.
        """
        self.assertTrue(
            self._xcopy_instance.copy_tree(
                source=self._directory,
                destinity="{}\\Copia_ignore".format(self._directory),
                overwrite=True,
                ignore_pattern=('not_copy', '*.bmp')
            )
        )

        self.assertFalse(
            os.listdir("{}\\Copia_ignore".format(self._directory)) == os.listdir(self._directory)
        )


if __name__ == '__main__':
    main()