# -*- coding: UTF-8 -*-
import wmi
from unittest import TestCase, main
from ..wmi_connector.connector import WmiConnect


class WmiConnectorTest(TestCase):

    def setUp(self):
        self._computer = '10.228.1.144'
        self._user = r"lmcorp\andre.matias"
        self._password = None
        self._namespace = 'root\cimv2'

    def test_if_connect_to_remote_server(self):

        remote = WmiConnect.connect(
            attemps=1,
            computer=self._computer,
            user=self._user,
            password=self._password,
            namespace=self._namespace
        )

        self.assertIsInstance(remote, wmi._wmi_namespace)

        self.assertRaises(
            Exception,
            WmiConnect.connect,
            attemps=0
        )

    def test_if_connect_localyr(self):

        remote = WmiConnect.connect(
            attemps=1
        )

        self.assertIsInstance(remote, wmi._wmi_namespace)

        self.assertRaises(
            Exception,
            WmiConnect.connect,
            attemps=0
        )


if __name__ is "__main__":
    main()
