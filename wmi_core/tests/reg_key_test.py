# -*- coding: UTF-8 -*-
from unittest import TestCase, main
from ..control.registries.Registry import RegKey


class TestRegKey(TestCase):

    def setUp(self):
        self._regkey_instance = RegKey(RegKey.HKEY_CURRENT_USER, r'SOFTWARE\7-zip')

    def test_get_hkey_name(self):
        """
        Teste para verificar o método get_hkey_name que retorna
        o nome da chave
        :return:
        """
        self.assertRaises(
            ValueError,
            self._regkey_instance.get_hkey_name,
            '1010101'
        )

        self.assertRaises(
            ValueError,
            self._regkey_instance.get_hkey_name,
            1010101
        )

        actual = self._regkey_instance.get_hkey_name(RegKey.HKEY_CURRENT_USER)
        expected = 'HKEY_CURRENT_USER'
        self.assertEqual(actual, expected)


if __name__ is "__main__":
    main()
