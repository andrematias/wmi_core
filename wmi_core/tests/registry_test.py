# -*- coding: utf-8 -*-

import wmi
import os
from unittest import TestCase, main
from ..control.registries.Registry import (
    Registry,
    RegKey,
    RegValue
)
import wmi_core.control.registries.RegistryExceptions as exRegistry


class ResgistryTest(TestCase):
    USER_NAME = r"lmcorp\andre.matias"
    USER_PASSWORD = None
    REMOTE_COMPUTER = '10.228.1.144'
    COMPUTER = r'localhost'
    NAMESPACE = r'root/default'

    def setUp(self):
        """
        Set variables and instances of class on test target
        :return:
        """
        self._wmi_namespace = wmi.WMI(
            computer=self.COMPUTER,
            namespace=self.NAMESPACE
        )

        self._wmi_namespace_remote = wmi.WMI(
            computer=self.REMOTE_COMPUTER,
            user=self.USER_NAME,
            password=self.USER_PASSWORD,
            namespace=self.NAMESPACE
        )
        self.__resgistry_instance = Registry(self._wmi_namespace)
        self.__resgistry_instance_remote = Registry(self._wmi_namespace_remote)
        self.subkey_local = r"SOFTWARE\Alps"
        self.subkey_remote = r"SOFTWARE\Wow6432Node\LM Sistemas\DataBase"
        self.file_path = r"C:\Users\Matias\Desktop\exportacao_registro.reg"
        self.file_path_remote = r"C:\Users\Matias\Desktop\exportacao_remota_registro.reg"

    def test_if_raise_NotValidWmiNamespaceInstance_exception(self):
        """
        Test if throw an NotValidWmiNamespaceInstance when
        create an new object of Registry class
        :return:
        """
        self.assertRaises(exRegistry.NotValidWmiNamespaceInstance, Registry, 'not_valid_wmi_instance')

    def test_private_method_check_reg_access(self):
        """
        Test if method check_reg_access return an
        instance of bool and current user have a
        permission to access the
        HKEY_CURRENT_USER\Software\7-Zip
        :return:
        """

        # Check a return type of method
        self.assertIsInstance(
            self.__resgistry_instance._Registry__check_reg_access(
                RegKey(RegKey.HKEY_CURRENT_USER, r"SOFTWARE\7-zip")
            ),
            bool
        )

        # Check if user have permission to access registry
        self.assertTrue(
            self.__resgistry_instance._Registry__check_reg_access(
                RegKey(
                    RegKey.HKEY_CURRENT_USER,
                    self.subkey_local
                )
            )
        )

    def test_check_reg_access_remote_computer(self):
        if self.REMOTE_COMPUTER is not None:
            self.assertTrue(
                self.__resgistry_instance_remote._Registry__check_reg_access(
                    RegKey(
                        RegKey.HKEY_LOCAL_MACHINE,
                        self.subkey_remote,
                        RegKey.KEY_WOW64_64KEY | RegKey.KEY_READ
                    )
                )
            )

    def test_get_keys_names_method(self):
        actual_list = self.__resgistry_instance.get_keys_names(
            RegKey(
                RegKey.HKEY_CURRENT_USER,
                self.subkey_local,
                RegKey.READ_CONTROL
            )
        )

        self.assertIsInstance(
            actual_list,
            list
        )

        for item in actual_list:
            self.assertIsInstance(item, RegKey)

    def test_get_value_name_method(self):
        self.assertIsInstance(
            self.__resgistry_instance.get_value_name(
                RegKey(
                    RegKey.HKEY_CURRENT_USER,
                    self.subkey_local
                )
            ),
            list
        )

    def test_get_entry_values(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            self.subkey_local
        )

        list_entries = self.__resgistry_instance.get_value_name(regkey)

        for value_name, value_type in [entry for entry in list_entries]:
            self.assertIsInstance(
                self.__resgistry_instance.get_reg_value(
                    regkey,
                    value_name,
                    value_type
                ),
                RegValue
            )

    def test_set_entry_values(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            self.subkey_local
        )

        regvalue = RegValue("Test", 'Ola Mundo :D', RegValue.REG_SZ)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

        regvalue = RegValue("DWORD", 0, RegValue.REG_DWORD)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

        regvalue = RegValue("BIN", [10, 1], RegValue.REG_BINARY)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

        regvalue = RegValue("EXPAND", '%systemroot%', RegValue.REG_EXPAND_SZ)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

        regvalue = RegValue("MULTI_SZ", ['str1', 'str2'], RegValue.REG_MULTI_SZ)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

        regvalue = RegValue("QWORD", 10, RegValue.REG_QWORD)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

    def test_set_entry_multi_string_values(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            self.subkey_local
        )

        regvalue = RegValue("Test", ['Ola', 'Mundo', 'Multi :D'], RegValue.REG_MULTI_SZ)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

    def test_set_entry_values_remotly(self):
        regkey = RegKey(
            RegKey.HKEY_LOCAL_MACHINE,
            self.subkey_remote,
            RegKey.KEY_WOW64_64KEY | RegKey.KEY_READ
        )

        regvalue = RegValue("Test", 'Ola Mundo :D', RegValue.REG_SZ)
        self.assertTrue(
            self.__resgistry_instance_remote.set_reg_value(
                regkey,
                regvalue
            )
        )

    def test_delete_entry_value_remotly(self):
        regkey = RegKey(
            RegKey.HKEY_LOCAL_MACHINE,
            self.subkey_remote
        )

        regvalue = RegValue("Test", 'Ola Mundo :D', RegValue.REG_SZ)
        self.assertTrue(
            self.__resgistry_instance_remote.delete_reg_value(regkey, regvalue)
        )

    def test_set_entry_values_change_value_of_registry(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            self.subkey_local
        )

        regvalue = RegValue("Test", 'Ola Mundo Atualizado :D', RegValue.REG_SZ)
        self.assertTrue(
            self.__resgistry_instance.set_reg_value(
                regkey,
                regvalue
            )
        )

    def test_delete_entry_value(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            self.subkey_local
        )

        regvalue = RegValue("Test", 'Ola Mundo :D', RegValue.REG_SZ)
        self.assertTrue(
            self.__resgistry_instance.delete_reg_value(regkey, regvalue)
        )

    def test_create_key(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            r"SOFTWARE\NovaChaveCriadaPeloRegistryPy"
        )

        self.assertTrue(
            self.__resgistry_instance.create_key(regkey)
        )

    def test_create_key_with_subkey(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            r"SOFTWARE\NovaChaveCriadaPeloRegistryPy\Nova"
        )

        self.assertTrue(
            self.__resgistry_instance.create_key(regkey)
        )

    def test_delete_key_recursively(self):
        regkey = RegKey(
            RegKey.HKEY_CURRENT_USER,
            r"SOFTWARE\NovaChaveCriadaPeloRegistryPy"
        )

        self.assertTrue(
            self.__resgistry_instance.delete_key(regkey)
        )

    def test_convert_string_to_expanded_string(self):
        actual = self.__resgistry_instance._Registry__string_to_reg_expanded(r"C:\users\Matias")
        expected = 'hex(2):43,00,3a,00,5c,00,75,00,73,00,65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,00,61,00,73,00'
        self.assertEqual(actual, expected)

    def test_convert_expanded_string_to_string(self):
        actual = self.__resgistry_instance._Registry__reg_expanded_to_string(
            'hex(2):43,00,3a,00,5c,00,75,00,73,00,65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,00,61,00,73,00'
        )
        expected = r"C:\users\Matias"
        self.assertEqual(actual, expected)

    def test_convert_int_to_reg_dword(self):
        actual = self.__resgistry_instance._Registry__int_to_reg_dword(10)
        expected = r"dword:0000000a"
        self.assertEqual(actual, expected)

        self.assertTrue(len(actual) == 8 + 6)

    def test_convert_reg_dword_to_int(self):
        actual = self.__resgistry_instance._Registry__reg_dword_to_int(r"000000a")
        expected = 10
        self.assertEqual(actual, expected)

        self.assertIsInstance(actual, int)

    def test_convert_list_integer_to_reg_binary_hex(self):

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__list_integer_to_reg_binary_hex,
            ['1', '0', '0']
        )

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__list_integer_to_reg_binary_hex,
            1
        )

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__list_integer_to_reg_binary_hex,
            ['Number']
        )

        actual = self.__resgistry_instance._Registry__list_integer_to_reg_binary_hex([1, 160, 0])
        expected = 'hex:01,a0,00'
        self.assertEqual(actual, expected)

        self.assertIsInstance(actual, str)

    def test_binary_hex_to_int(self):
        actual = self.__resgistry_instance._Registry__reg_binary_hex_to_list_integer('hex:31,30,30')
        expected = [49, 48, 48]
        self.assertEqual(actual, expected)

        self.assertIsInstance(actual, list)

    def test_list_string_to_reg_multi_string(self):
        actual = self.__resgistry_instance._Registry__list_string_to_reg_multi_string([r"C:\Users", r"D:\Records"])
        expected = 'hex(7):43,00,3a,00,5c,00,55,00,73,00,65,00,72,00,73,00,00,00,00,00,  ' \
                   '44,00,3a,00,5c,00,52,00,65,00,63,00,6f,00,72,00,64,00,73,00,00,00,00,00'
        self.assertEqual(actual, expected)

    def test_reg_multi_string_to_list_string(self):
        hex_string = 'hex(7):43,00,3a,00,5c,00,55,00,73,00,65,00,72,00,73,00,00,00,00,00,  ' \
                   '44,00,3a,00,5c,00,52,00,65,00,63,00,6f,00,72,00,64,00,73,00,00,00,00,00'
        actual = self.__resgistry_instance._Registry__reg_multi_string_to_list_string(hex_string)
        expected = [r"C:\Users", r"D:\Records"]
        self.assertEqual(actual, expected)

        self.assertIsInstance(actual, list)

        self.assertRaises(
            ValueError,
            self.__resgistry_instance._Registry__reg_multi_string_to_list_string,
            'valor invalido'
        )

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__reg_multi_string_to_list_string,
            10
        )

    def test_string_to_reg_qword(self):

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__string_to_reg_qword,
            10
        )

        self.assertRaises(
            exRegistry.MaxLength,
            self.__resgistry_instance._Registry__string_to_reg_qword,
            'abcdef12345678904395038'
        )

        self.assertRaises(
            TypeError,
            self.__resgistry_instance._Registry__string_to_reg_qword,
            'ghij123'
        )

        actual = self.__resgistry_instance._Registry__string_to_reg_qword('abcdef12345678')
        expected = r"hex(b):78,56,34,12,ef,cd,ab,00"
        self.assertEqual(actual, expected)

    def test_reg_qword_to_string(self):
        self.assertRaises(
            ValueError,
            self.__resgistry_instance._Registry__reg_qword_to_string,
            10
        )

        self.assertRaises(
            ValueError,
            self.__resgistry_instance._Registry__reg_qword_to_string,
            '78,56,34,12,ef,cd,ab,00'
        )

        actual = self.__resgistry_instance._Registry__reg_qword_to_string('hex(b):78,56,34,12,ef,cd,ab,00')
        expected = r"abcdef12345678"

        self.assertEqual(actual, expected)

    def test_convert_reg_sz_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw("200,350,500,1500,2500", RegValue.REG_SZ),
            '"200,350,500,1500,2500"'
        )

    def test_convert_reg_dword_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw(11, RegValue.REG_DWORD),
            'dword:0000000b'
        )

    def test_convert_reg_qword_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw('abcdef12345678', RegValue.REG_QWORD),
            'hex(b):78,56,34,12,ef,cd,ab,00'
        )

    def test_convert_reg_expand_sz_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw(r"C:\users\Matias", RegValue.REG_EXPAND_SZ),
            'hex(2):43,00,3a,00,5c,00,75,00,73,00,65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,00,61,00,73,00'
        )

    def test_convert_reg_binary_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw([16, 160], RegValue.REG_BINARY),
            'hex:10,a0'
        )

    def test_convert_reg_multi_sz_value_to_raw(self):
        self.assertEqual(
            self.__resgistry_instance.convert_value_to_raw(['valor1', 'valor2'], RegValue.REG_MULTI_SZ),
            'hex(7):76,00,61,00,6c,00,6f,00,72,00,31,00,00,00,00,00,  76,00,61,00,6c,00,6f,00,72,00,32,00,00,00,00,00'
        )

    def test_convert_raw_to_reg_multi_sz_value(self):
        hex_string = 'hex(7):43,00,3a,00,5c,00,55,00,73,00,65,00,72,00,73,00,00,00,00,00,  ' \
                     '44,00,3a,00,5c,00,52,00,65,00,63,00,6f,00,72,00,64,00,73,00,00,00,00,00'
        actual = self.__resgistry_instance.convert_raw_to_value(hex_string, RegValue.REG_MULTI_SZ)
        expected = [r"C:\Users", r"D:\Records"]

        self.assertEqual(actual, expected)

    def test_convert_raw_reg_binary_to_value(self):
        self.assertEqual(
            self.__resgistry_instance.convert_raw_to_value('hex:10,a0', RegValue.REG_BINARY),
            [16, 160]
        )

    def test_convert_raw_reg_expand_sz_value(self):
        hex_string = 'hex(2):43,00,3a,00,5c,00,75,00,73,00,65,00,72,00,73,00,5c,00,4d,00,61,00,74,00,69,00,61,00,73,00'
        actual = self.__resgistry_instance.convert_raw_to_value(hex_string, RegValue.REG_EXPAND_SZ)
        expected = r"C:\users\Matias"

        self.assertEqual(actual, expected)

    def test_convert_raw_reg_qword_value(self):
        self.assertEqual(
            self.__resgistry_instance.convert_raw_to_value('hex(b):78,56,34,12,ef,cd,ab,00', RegValue.REG_QWORD),
            'abcdef12345678'
        )

    def test_convert_raw_to_reg_sz_value(self):
        self.assertEqual(
            self.__resgistry_instance.convert_raw_to_value('"200,350,500,1500,2500"', RegValue.REG_SZ),
            "200,350,500,1500,2500"
        )

    def test_convert_raw_reg_dword_value(self):
        self.assertEqual(
            self.__resgistry_instance.convert_raw_to_value('dword:0000000b', RegValue.REG_DWORD),
            11
        )

    def test_export_reg(self):

        regkey = RegKey(RegKey.HKEY_CURRENT_USER, self.subkey_local)

        self.assertRaises(
            TypeError,
            self.__resgistry_instance.export_reg,
            None,
            self.file_path
        )

        self.assertRaises(
            NotADirectoryError,
            self.__resgistry_instance.export_reg,
            regkey,
            'diretorio_invalido'
        )

        self.assertTrue(
            self.__resgistry_instance.export_reg(regkey, self.file_path)
        )

    def test_export_reg_remotly(self):

        regkey = RegKey(RegKey.HKEY_LOCAL_MACHINE, self.subkey_remote)

        self.assertRaises(
            TypeError,
            self.__resgistry_instance_remote.export_reg,
            None,
            self.file_path_remote
        )

        self.assertRaises(
            NotADirectoryError,
            self.__resgistry_instance_remote.export_reg,
            regkey,
            'diretorio_invalido'
        )

        self.assertTrue(
            self.__resgistry_instance_remote.export_reg(regkey, self.file_path_remote)
        )

    def test_load_registry(self):

        self.assertRaises(
            NotADirectoryError,
            self.__resgistry_instance.load_registry,
            'diretorio_invalido'
        )

        self.assertRaises(
            FileNotFoundError,
            self.__resgistry_instance.load_registry,
            os.path.dirname(self.file_path)
        )

        actual = self.__resgistry_instance.load_registry(self.file_path)
        self.assertIsInstance(actual, list)

    def test_import_registry(self):
        actual = self.__resgistry_instance.import_registry(self.file_path)
        self.assertTrue(actual)

    def test_import_registry_remotly(self):
        actual = self.__resgistry_instance_remote.import_registry(self.file_path_remote)
        self.assertTrue(actual)


if __name__ is "__main__":
    main()
