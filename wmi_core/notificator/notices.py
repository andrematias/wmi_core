# -*- coding: UTF-8 -*-
from .notification import AbstractNotice


class Success(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_SUCCESS)


class Error(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_ERROR)


class Info(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_INFO)


class Alert(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_ALERT)


class Debug(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_DEBUG)


class Critical(AbstractNotice):
    def __init__(self, an_notice: str) -> None:
        super().__init__(an_notice, AbstractNotice.NOTICE_TYPE_CRITICAL)
