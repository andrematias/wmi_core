# -*- coding: UTF-8 -*-
from abc import ABC, abstractmethod


class AbstractNotice(ABC):
    """
    Classe Abstract de uma noticia para o notificador dos domínios
    """
    NOTICE_TYPE_INFO = "Info"
    NOTICE_TYPE_DEBUG = "Debug"
    NOTICE_TYPE_ERROR = "Error"
    NOTICE_TYPE_CRITICAL = "Critical"
    NOTICE_TYPE_ALERT = "Alert"
    NOTICE_TYPE_SUCCESS = "Success"

    @abstractmethod
    def __init__(self, an_message: str,  notice_type: str) -> None:
        self.__message = an_message
        self.__notice_type = notice_type

    def __str__(self) -> str:
        """
        Representação da classe notice como str
        :return:    str
        """
        return "Notificação - [%s]: %s" % (self.type, self.message)

    @property
    def type(self) -> str:
        return self.__notice_type

    @property
    def message(self) -> str:
        return self.__message


class Notificator(ABC):
    """
    Classe NotificatorService manuzeia uma lista com
    erros que ocorrem durante uma operação de Mapper
    """

    def __init__(self) -> None:
        self.__notices = []

    def add(self, an_notice: AbstractNotice) -> None:
        self.__notices.append(an_notice)

    def get_notices(self):
        return self.__notices

    def has_notices(self):
        return len(self.__notices) > 0

    def clear(self):
        self.__notices.clear()
