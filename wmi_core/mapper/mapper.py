import pywintypes
from win32wnet import (
    NETRESOURCEType,
    WNetAddConnection2,
    WNetCancelConnection2,
    WNetOpenEnum,
    WNetEnumResource
)
from win32netcon import (
    RESOURCETYPE_DISK,
    CONNECT_TEMPORARY,
    RESOURCE_CONNECTED,
    RESOURCETYPE_ANY,
    RESOURCEUSAGE_CONNECTABLE
)
from typing import List
from .MapperExceptions import (
    ConnectionException,
    DisconectException
)


class NetResource(object):
    """
    Classe para criar um NETRESOURCEType
    Esta classe é responsável por informar a Mapper o qual endereço e onde
    sera mapeado.
    """

    def __new__(cls, shared_path: str, letter: str = None, dw_type: int = RESOURCETYPE_DISK) -> NETRESOURCEType:
        """
        Construtor da classe para montar um NETRESOURCEType

        :param shared_path:         O caminho compartilhado a ser mapeado
        :param letter:              Uma letra de unidade do sistema para
                                    montar o mapeamento
        :param dw_type:             O tipo de conexão
        :return:                    Uma instancia de NETRESOURCEType com as
                                    principais propriedades prrenchidas
        """
        _net_resource = NETRESOURCEType()
        _net_resource.dwType = dw_type

        if letter is not None and ':' not in letter:
            letter = letter + ':'

        _net_resource.lpLocalName = letter
        _net_resource.lpRemoteName = shared_path

        return _net_resource


class Mapper(object):
    """
    :ref: https://msdn.microsoft.com/en-us/library/windows/desktop/aa385413(v=vs.85).aspx
    """

    def __init__(
            self,
            resource: NETRESOURCEType,
            password: str = None,
            user: str = None,
            type_connection: int = CONNECT_TEMPORARY
    ) -> None:
        """
        Inicializador da classe

        :param resource:        Uma instancia de NetResource|NETRESOURCEType
        :param password:        Uma str com uma senha válida para user
        :param user:            Uma str com o nome de usuário. Se estiver em um dminínio
                                será necessário inclui-lo e separa-lo por "\"
        :param type_connection: Um int com o tipo de conexão
        """

        self._resource = resource
        self._password = password
        self._user = user
        self._type_connection = type_connection

    def __connect(self, resource: NETRESOURCEType, password: str, user: str, type_connection: int) -> bool:
        """
        Método privado para conexão com um servidor
        :param resource:        Uma instancia de NetResource|NETRESOURCEType
        :param password:        Uma str com uma senha válida para user
        :param user:            Uma str com o nome de usuário. Se estiver em um dminínio
                                será necessário inclui-lo e separa-lo por "\"
        :param type_connection: Um int com o tipo de conexão
        :return:                bool
        """
        conn = WNetAddConnection2(resource, password, user, type_connection)
        if conn is not None:
            raise ConnectionException(
                'Falha na conexão com o compartilhamento %s' % resource.lpRemoteName
            )
        return True

    def map(self) -> None:
        """
        Método para autenticar nos servidores listados em self._list_resources
        Caso ocorra falhas será armazenado em uma lista de erros
        :return:    None
        """
        return self.__connect(self._resource, self._password, self._user, self._type_connection)


    @classmethod
    def unmap_an(cls, a_resource: NETRESOURCEType) -> bool:
        """
        Desconecta o mapeamento realizado para um NetResource
        :return: bool
        """

        if isinstance(a_resource, NETRESOURCEType):
            try:
                connection = a_resource.lpRemoteName

                if a_resource.lpLocalName is not None:
                    connection = a_resource.lpLocalName

                WNetCancelConnection2(connection, 1, 1)
                return not cls.is_mapped(a_resource)
            except (DisconectException, pywintypes.error):
                pass
    @classmethod
    def unmap(cls) -> bool:
        """
        Desconecta o mapeamento realizado para todos os NetResources
        :return: bool
        """
        for resource in cls.list():
            cls.unmap_an(resource)

        return len(cls.list()) == 0

    @classmethod
    def list(cls) -> List[NETRESOURCEType]:
        """
        Recupera uma lista atual de NetResource do sistema
        :return:        uma lista com NETRESOURCEType
        """
        return WNetEnumResource(
            WNetOpenEnum(
                RESOURCE_CONNECTED,
                RESOURCETYPE_ANY,
                RESOURCEUSAGE_CONNECTABLE,
                None
            ),
            0
        )

    @classmethod
    def is_mapped(cls, resource: NETRESOURCEType) -> bool:
        """
        Verifica se um NetResource/NETRESOURCEType/NETRESOURCE/PyNETRESOURCE
        esta mapeado.
        :type resource: NETRESOURCEType
        :return:        bool
        """
        if not isinstance(resource, NETRESOURCEType):
            raise TypeError(
                'O tipo informado em is_mapped não é um NETRESOURCEType válido'
            )

        return resource.lpRemoteName in [mapped.lpRemoteName for mapped in cls.list()]
