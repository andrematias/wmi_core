# -*- coding: UTF-8 -*-
import os
import shutil
import glob


class XCopy(object):

    def __file_exists(self, file):
        """
        Verifica se o parametro file é um arquivo.
        :param file: O caminho completo para o arquivo
        :return: True se for um arquivo
        """

        if os.path.isfile(file):
            return True

    def __get_files_by_wildcard(self, path_with_wildcard):
        return glob.glob(path_with_wildcard)

    def copy_file(self, file_path, destinity, overwrite=True):
        """
            Método de cópia de arquivos

            :file_path              Um caminho absoluto para um arquivo existente
                                    ou um wildcard. E.g.: *.txt
            :destinity              O destino do novo arquivo copiado
            :overwrite              Caso True força a substituição do arquivo
            :FileNotFoundError      Caso o arquivo file_path não existir

            :return: bool           True se copiar o arquivo.
        """
        global file_path_list, destinity_list
        file_path_list = [file_path]
        destinity_list = []

        if '*' in file_path:
            file_path_list = self.__get_files_by_wildcard(file_path)
        elif not self.__file_exists(file_path):
            raise FileNotFoundError("O arquivo {} não existe".format(file_path))

        if overwrite is False and self.__file_exists(destinity):
            raise FileExistsError("O Arquivo {} já existe".format(destinity))

        counter = 0
        for file in file_path_list:
            if os.path.isdir(destinity):
                destinity_list.append(os.path.join(destinity, os.path.basename(file)))
                shutil.copyfile(file, destinity_list[counter])
                counter += 1
            else:
                shutil.copyfile(file, destinity)

        return True

    def copy_tree(self, source, destinity, symlynks=False, ignore_pattern=(), overwrite=False):
        """
            Método para copiar arquivos de um diretório, recursivamente.

            :param source:             Um diretório de onde será realizado a cópia
            :param destinity:          O diretório para onde será copiado os arquivos
                                         de :source
            :param symlynks:           Caso True sera copiado os links simbólicos que
                                         estão dentro do diretório de :source
            :param ignore_pattern:     Callback com os padrões a ser ignorados na cópia
                                         dos arquivos e diretórios de :source
            :param overwrite:          Booleano para sobrescrever os arquivos existente,
                                        o valor default é False
            :raise NotADirectoryError: Exceção lançada quando :source ou :destinity não
                                         for um diretório.
        """

        if not os.path.isdir(source):
            raise NotADirectoryError("o diretório informado não é um diretório valido")

        if source == destinity:
            raise shutil.SameFileError("O diretório {} é o mesmo de: {}".format(destinity, source))

        if overwrite:
            shutil.rmtree(destinity, True)

        shutil.copytree(
            source,
            destinity,
            symlinks=symlynks,
            ignore=shutil.ignore_patterns(*ignore_pattern),
            copy_function=self.copy_file
        )
        return True
