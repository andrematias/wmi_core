from setuptools import setup, find_packages

find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "_tests.*", "tests"])

setup(
    name='wmi_core',
    version='1.0.0.dev1',
    packages=['wmi_core', 'wmi_core.tests', 'wmi_core.copier', 'wmi_core.mapper', 'wmi_core.control',
              'wmi_core.control.services', 'wmi_core.control.processes', 'wmi_core.control.registries',
              'wmi_core.metadata', 'wmi_core.notificator', 'wmi_core.wmi_connector'],
    url='https://Andrematias@bitbucket.org/Andrematias/wmi_core',
    license='MIT',
    author='André Matias',
    author_email='dev.andrematias@gmail.com',
    description='Core de utilidades do WMI',
    install_requires=[
        'pywin32 == 223',
        'wmi == 1.4.9'
    ],
    python_requires='>=3.6.*'

)
